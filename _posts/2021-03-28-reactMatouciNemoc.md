---
layout: post
title: "REACT: O matoucí nemoci"
date: 2021-03-28T00:00:00
---
Protože tenhle blog nemá komentáře, tak jsem samozřejmě nedostal žádnou reakci na svoji předešlou práci. To mi nevadí, protože si dovedu živě představit, jaké názory bych se dozvěděl. Teď musím jenom doufat, že jsem se alespoň s nějakou otázkou nebo postřehem trefil 😁.

# Jsi strašně sobecký!

To ti není líto všech těch lidí, kteří kvůli covidu umřeli? Co kdyby to byla tvoje babička? Neměli bychom se je snažit zachránit? Lepší je když se trochu omezíš a zachráníš tím životy.

---

Je mi jich hodně líto a soucítím se všemi, které ta nemoc připravila o někoho blízkého. Mnohem víc líto je mi ale těch lidí, kteří měli svůj obchůdek, svoji malou firmu a jenom chtěli žít spokojený život a stát jim to zakázal. Jsem strašně smutný když se jednou za čas podívám do města a vidím, jak se tihle lidé snaží udělat alespoň něco, třeba otevřít výdejní okénko nebo prodávat přes internet, jak se nadějně chytají prohlášení vlády o rozvolňování. Lituji všechny malé děti, kterým ničíte životy, ničíte jejich přátelství, zájmy a naději na budoucnost. Lituji všechny, kteří mají milého ve druhém okresu a třeba jen přes kopec daleko. Myslím si, že tenhle současný přístup k epidemii je strašně zlý a stejně se najdou lidé, kteří to nevidí nebo vidět nechtějí a myslí si, jak je to morálně správné a nutné. To se mi nelíbí.

# Vyjmenované zákazy jsou něco jiného

Všimnul jsem si, že u vyjmenovaných zákazů chybí kouření, že by proto, že by to bylo covidu moc podobné ;)? Zákaz kouření na autobusové zastávce je celkem jasný, protože tím obtěžuješ ostatní lidi. A covid je taková neviditelná cigareta, u které nevíš, jestli ji náhodou nemáš. Proto je třeba omezit všechny, i ty co ho zrovna nemají.

---

No, od toho jsou tady přece respirátory a roušky! Ty jsou tady od toho, aby i nemocný člověk nikoho nenakazil a mohl být v klidu na srazu nebo v obchodě . Nejsou sice dokonalé, ale existují i sofistikovanější masky nebo by mohli existovat, kdyby se do jejich výzkumu a výroby vrazilo hodně peněz. Dokonale[^dokonale] filtrující masku si dokážu představit i já, a to nejsem ani zdaleka materiální chemik nebo odborník na viry. Je skvělé, že naše vláda utrácí za pandemii tak neuvěřitelně peněz, že by maska mohla mít i slušný rozpočet a ještě by nám zbylo.

[^dokonale]: dokonalá, tedy 100% účinnost je asi nerealistická, ale 99+% (~) už za dokonalé považuji.

# Chybí ti osobní zkušenost.

Měl by jsi se podívat do přeplněné nemocnice a vidět, kolik lidí tam umírá. Pak by jsi změnil názor.

---

První verze původního článku vznikla právě proto, že jsem nemoc měl a byl jsem z ní celkem zmatený. Všichni o ní pořád mluvili a báli se. Mě jenom bolelo v krku, měl jsem potom rýmu a to je všechno. Ještě vyšel pozitivní test. Celkem trapný (samozřejmě naštěstí!). U rodičů, kamarádů a jejich rodičů to bylo stejné, jen to chytli jindy a jinde. Asi bych to mohl dokázat i statistikou, vytvořit poměr nakažení ku těžce nemocní u jednotlivých věkových skupin, ale spokojím se se závěrem, že pokud chytnete nemoc a nejste moc staří/jinak nemocní, tak budete mít *spíš* mírný průběh a tím pádem se není čeho bát.
Pokud jsem vás o neškodnosti přeplněným nemocnic nepřesvědčil minule, tak to nedokážu ani teď. Velice snadno si můžete myslet, že je to prostě špatně a to proto, že máte jinou morálku. Získali jste ji zkušenostmi, výchovou nebo tak nějak a u každého člověk je trochu jiná. Proto je to i celkem zábavné, bavit se o společenských tématech a mít v tom jasno. Já svoji morálku v přemýšlení nepoužívám a to proto, že by se výsledek mého přemýšlení mohl lišit, třeba jen tím, že bych se narodil v jiném státě nebo měl jinou učitelku ve škole.
O správné morálce tahle esej ale není, takže váš pohled na to, že přeplněné nemocnice jsou nemorální nezměním. Pravděpodobně bych ho nezměnil ani kdybych se o to snažil, neboť morálka je zvlášť v dospělých lidech pevně zakotvena.

# Co ta otázka na konci?

Pro připomenutí: Jak se zachovat, pokud vám dá někdo nesmyslný příkaz?[^myslet] A mění na vašem rozhodnutí něco, když je to stát a téma se týká covidu?

[^myslet]: (nebo si to alespoň myslíte)

---

Nebudeme si nic nalhávat, protože se každý člověk zachová podobně. Zváží nevýhody a výhody plynoucí z porušení nebo poslechnutí nařízení, riziko že na něj přijdou a poté si vybere takovou možnost, která ho udělá nejspokojenějšího. V praxi to vypadá tak, že lidé dodržují taková opatření, která nejsou tolik otravná, ty které dávají smysl a taky ta, která jsou vymáhána a pokutována. Na ostatní více či méně kašle, nebo si je pro sebe morálně ospravedlňuje.

# Dík za hezký článek!

Taky mě těšilo a zase příště 😉!

