---
layout: post
title: "O matoucí nemoci"
date: 2021-03-03T00:00:00
assets: /assets/
---

Covid je v současné době velmi diskutované téma a asi jste si všimli, že si lidé v diskuzích a ve vašem životě hodně protiřečí a ještě k tomu tahají do svých úvah spousty emocí. Já vám v tomhle nepomůžu a ani nepřijdu na nějaký jednuduchý způsob jak všechny zachránit. Přesto vám ale chci ukázat jiný pohled na svět a napsat, jak se v přemýšlení o covidu vyhnout alespoň nějakým chybám.

Během údajů o velkém počtu nakažených a třeba i mrtvých se zapomíná na to, že se velká část lidí nakazila dobrovolně a dala přednost sledování svého osobního štěstí před potenciální nákazou. To jsou všichni samostatní senioři, distanční studenti a lidé, kteří mohou pracovat z domova. U některých dalších lidí je to trochu komplikovanější a určitě se najdou tací, kteří si nemohou vybrat a musí riziko podstoupit a třeba se i nakazit. To ale nemění nic na tom, že není důvod litovat osoby z první skupiny, protože si tuto cestu sami vybrali a v případě přeplněných nemocnic si zaslouží nést následky svého rozhodnutí. Přijde mi hloupé omezovat Svobodu ostatních, jenom kvůli tomu, že se někdo chová sám vůči sobě nezodpovědně a nechce změnit svůj přístup k životu i přes zjevné nebezpečí, které mu tento přístup přináší. O to víc, když naprosté většině lidí žádné nebezpečí nehrozí a nákaza se u nich projeví jako obyčejné nachlazení.

Největší část nakažených v nemocnicích tvoří údajně staří lidé, jejichž imunita si s virem nedokáže poradit sama. Právě u nich je ještě důležitější zdůraznit, že se nakazili **dobrovolně**. Dostávají od státu důchod a kdyby chtěli, nemusí se díky Moderní době setkávat vůbec s nikým a být doma v bezpečí a izolaci. Stejně to ale nedělají a pokouší své štěstí. (jsou senioři, kteří se neobejdou bez péče druhé osoby, sdílí domácnost, atd. a pak je jejich izolace obtížnější).

Někteří lidé se jako čert kříže bojí přeplněných nemocnic a mají strašný strach, že by zdravotníci někomu nemohli pomoci. Myslí si, že je lepší uměle zpomalit šíření viru a tak se vyhnout morálně nepříjemné situaci, kdy se někdo nechá umřít jenom kvůli tomu, že není místo. Hezky znějící myšlenka 😊. Stejnou argumentací by se ale dalo zakázat skoro cokoli. Mohli byste zakázat cestování autem, protože na silnicích zemře mnoho nevinných lidí, kteří by jinak žili svůj šťastný život dál. Mohli byste zakázat alkohol, protože opilí lidé často ubližují a to i vlastní rodině a svým nejbližším. Zakázal bych interakci mezi dětmi a vyřešil všechnu šikanu. A zakázal bych i volné šíření myšlenek a uchránil všechny lidi před falešnými informacemi a jejich tragickými následky.

Asi i těm největším zastáncům restrikcí je jasné, že argumentace v předešlém odstavci je šílená a zakázání vyjmenovaných věcí by způsobilo víc problémů než užitku. Bojím se, že je to tak i v případě lockdownu a že platíme neuvěřitelně vysokou cenu za záchranu lidí, kteří si to nezaslouží a sami si vybrali svoji životní cestu. (mimochodem po první přeplněné nemocnici si každý člověk rozmyslí své chování i bez jakéhokoli nařízení ze strany státu 🙂)

Tímhle vším neříkám, že by se člověk neměl chovat hezky vůči ostatním. Přijde mi rozumné nechodit s příznaky ven a informovat kamarády až nemoc chytnu. Ale rozhodně nemá smysl lidem vnucovat zdraví a brát jim jejich přírodou danou Svobodu, kterou by měl stát chránit a ne ničit a omezovat.
	
Na tuto úvahu navazuje mnohem praktičtější otázka a to, jestli je správné někoho poslouchat, pokud víme, že se chová nerozumně? A co když to je náhodou stát, ve kterém žiji? Odpověď na tyto otázky zatím neznám a nechci se unáhlit ve své volbě, protože to má narozdíl od zbytku textu potenciál ovlivnit můj život.
